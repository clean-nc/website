#!/bin/sh

# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

set -e

NITRILE_DIR="$HOME/.nitrile"
NITRILE_BIN="$NITRILE_DIR/bin"
NITRILE_PACKAGES="$NITRILE_DIR/packages"

if [ -e "$NITRILE_PACKAGES/nitrile" ]; then
	echo "$NITRILE_PACKAGES/nitrile already exists."
	echo "Remove $NITRILE_DIR to create a clean slate."
	exit 1
fi

NITRILE_PACKAGE_CONTENT="$(curl -s https://clean-lang.org/api/packages/nitrile)"

if [ $# -eq 1 ]; then
	JQ_SCRIPT_DOWNLOAD_URL=".versions[\"$1\"].targets.\"linux-x64\".url"
	NITRILE_VERSION=$1
else
	JQ_SCRIPT_DOWNLOAD_URL='.versions[.latest_version].targets."linux-x64".url'
	JQ_SCRIPT_VERSION='.latest_version'
	NITRILE_VERSION="$(printf '%s' "$NITRILE_PACKAGE_CONTENT" | jq -r "$JQ_SCRIPT_VERSION")"
fi

NITRILE_DOWNLOAD_URL="$(printf '%s' "$NITRILE_PACKAGE_CONTENT" | jq -r "$JQ_SCRIPT_DOWNLOAD_URL")"

mkdir -p "$NITRILE_BIN"
mkdir -p "$NITRILE_PACKAGES/nitrile/$NITRILE_VERSION-linux-x64"

curl -# "$NITRILE_DOWNLOAD_URL" | tar xzp --strip-components=1 -C "$NITRILE_PACKAGES/nitrile/$NITRILE_VERSION-linux-x64"

for prog in "$NITRILE_PACKAGES"/nitrile/$NITRILE_VERSION-linux-x64/bin/*; do
	prog="$(basename "$prog")"
	ln -s "../packages/nitrile/$NITRILE_VERSION-linux-x64/bin/$prog" "$NITRILE_BIN/$prog"
done

echo "All done."
echo
echo "Add $NITRILE_BIN to your PATH to start using nitrile."
