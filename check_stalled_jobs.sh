#! /bin/bash
# Usage: check_stalled_jobs EMAIL_ADDRESS

cd "$(dirname "$0")"

mysql_host="$(grep '^MYSQL_HOST=' .env | cut -d'=' -f2)"
mysql_user="$(grep '^MYSQL_USER=' .env | cut -d'=' -f2)"
mysql_pass="$(grep '^MYSQL_PASS=' .env | cut -d'=' -f2)"
mysql_db="$(grep '^MYSQL_DB=' .env | cut -d'=' -f2)"

# Get the number of unfinished jobs which are older than 10 minutes.
nr_stalled_jobs=$(mysql -h "$mysql_host" -u "$mysql_user" -p"$mysql_pass" "$mysql_db" -s -e "SELECT COUNT(*) FROM jobs WHERE created < DATE_SUB(NOW(), INTERVAL 10 MINUTE) AND isNull(finished);")
if [ "$nr_stalled_jobs" = 0 ]
then
	echo "no stalled jobs detected"
else
	echo "stalled jobs detected!"
	echo "$nr_stalled_jobs stalled job(s) have been detected." | mail -s "clean-lang.org: stalled jobs detected" -a "Content-Type: text/plain; charset=UTF-8" "$1"
fi
