/**
 * Copyright 2021-2024 the authors (see README.md).
 *
 * This file is part of the Clean website.
 *
 * The Clean website is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * The Clean website is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the Clean website. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `jobs` (
  `id` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finished` timestamp NULL DEFAULT NULL,
  `metadata` varchar(1023) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `packages` (
  `id` smallint NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `deprecated` tinyint(1) NOT NULL DEFAULT '0',
  `maintainer` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `contact_email` varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `license` varchar(31) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `readme` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `changelog` text CHARACTER SET utf8 COLLATE utf8_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `package_versions` (
  `package_id` smallint NOT NULL,
  `version` varchar(10) NOT NULL,
  `git_ref` varchar(63) NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `platform` enum('linux','mac','windows','any') NOT NULL,
  `architecture` enum('arm64','x86','x64','arm','intel','32bit','64bit','any') NOT NULL,
  `url` varchar(1023) NOT NULL,
  `dependencies` varchar(1023) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `package_versions`
  ADD UNIQUE KEY `package_id` (`package_id`,`version`,`platform`,`architecture`) USING BTREE;


ALTER TABLE `jobs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

ALTER TABLE `packages`
  MODIFY `id` smallint NOT NULL AUTO_INCREMENT;


ALTER TABLE `package_versions`
  ADD CONSTRAINT `package_versions_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;
