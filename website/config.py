# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

from dotenv import dotenv_values
import os

print('Loading config...')
config = dotenv_values('.env') | os.environ
config['HTTP_PORT'] = int(config['HTTP_PORT']) if 'HTTP_PORT' in config else 80
config['JOB_TIMER'] = int(config['JOB_TIMER'])
config['RUN_JOBS'] = config['RUN_JOBS'] == "true" if 'RUN_JOBS' in config else True
config['USE_MARIADB'] = config['USE_MARIADB'] == "true" if 'USE_MARIADB' in config else False

def __getattr__(name):
    if name.upper() in config:
        return config[name.upper()]
    raise AttributeError(f"no config value '{name}'")
