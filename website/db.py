# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

import json
import mariadb
import mysql.connector
from semver import VersionInfo
import time

import config
import jobs

class DatabaseCursor:
    def __init__(self, db):
        self.db = db

    def __enter__(self):
        self.cursor = self.db.cursor()
        return self.cursor

    def __exit__(self, type, value, traceback):
        self.cursor.close()

# Decorator for use in Database
def withcursor(func):
    def inner(self, *args, **kwargs):
        with self.cursor() as cursor:
            return func(self, cursor, *args, **kwargs)

    return inner

class Database:
    def __enter__(self):
        try:
            connect = mariadb.connect if config.use_mariadb else mysql.connector.connect
            self.db = connect(
                    host=config.mysql_host,
                    user=config.mysql_user,
                    passwd=config.mysql_pass,
                    database=config.mysql_db
                )
            return self
        except Exception as e:
            print(e)
            return self

    def __exit__(self, type, value, traceback):
        self.db.close()

    def cursor(self):
        return DatabaseCursor(self.db)

    def get_package_versions(self, cursor, package_id, with_dependencies=False):
        dependencies = ', dependencies' if with_dependencies else ''
        select_versions = f'''SELECT
                version, git_ref, created, platform, architecture, url {dependencies}
                FROM package_versions
                WHERE package_id=%s
                ORDER BY created ASC'''
        cursor.execute(select_versions, (package_id,))

        latest_version = None
        parsed_latest_version = None

        versions = {}
        for version_info in cursor.fetchall():
            version = version_info[0]
            git_ref = version_info[1]
            created = version_info[2]
            platform = version_info[3]
            architecture = version_info[4]
            url = version_info[5]

            parsed_version = VersionInfo.parse(version)
            if latest_version is None or parsed_version > parsed_latest_version:
                latest_version = version
                parsed_latest_version = parsed_version

            if version not in versions:
                versions[version] = {'created': int(time.mktime(created.timetuple())), 'targets': {}}

            record = {'url': url}
            if git_ref is not None:
                record['git_ref'] = git_ref
            if with_dependencies:
                record['dependencies'] = json.loads(version_info[6])

            versions[version]['targets'][platform+'-'+architecture] = record

        return latest_version, versions

    @withcursor
    def get_packages(self, cursor,
            with_dependencies=False, with_full_metadata=False):
        fields = 'id, name, url, deprecated, maintainer, license, description'
        fields += ', readme, changelog'
        select_packages = f'SELECT {fields} FROM packages'
        cursor.execute(select_packages)

        results = []
        for result in cursor.fetchall():
            latest_version, versions = self.get_package_versions(
                    cursor, result[0], with_dependencies=with_dependencies)

            record = {
                    'id': result[0],
                    'name': result[1],
                    'url': result[2],
                    'deprecated': result[3] == 1,
                    'maintainer': result[4],
                    'license': result[5],
                    'description': result[6],
                    'latest_version': latest_version,
                    'versions': versions
            }

            if with_full_metadata:
                record['readme'] = result[7]
                record['changelog'] = result[8]

            results.append(record)

        return results

    @withcursor
    def get_package(self, cursor, with_versions=False, **kwargs):
        select_package = 'SELECT id, name, url, deprecated, maintainer, license, description, readme, changelog FROM packages WHERE '
        values = None

        if 'id' in kwargs:
            select_package += 'id=%s'
            values = (kwargs['id'],)
        elif 'name' in kwargs:
            select_package += 'name=%s'
            values = (kwargs['name'],)
        else:
            raise AttributeError('id or name must be given')

        cursor.execute(select_package, values)

        result = cursor.fetchone()
        if result is None:
            return None

        record = {
            'id': result[0],
            'name': result[1],
            'url': result[2],
            'deprecated': result[3] == 1,
            'maintainer': result[4],
            'license': result[5],
            'description': result[6],
            'readme': result[7],
            'changelog': result[8]
        }

        if with_versions:
            latest_version, versions = self.get_package_versions(
                    cursor, result[0], with_dependencies=True)
            record['latest_version'] = latest_version
            record['versions'] = versions

        return record

    @withcursor
    def create_package(self, cursor, name, url):
        insert_package = 'INSERT INTO packages (name, url) VALUES (%s,%s)'
        cursor.execute(insert_package, (name, url))
        self.db.commit()
        return cursor.lastrowid

    @withcursor
    def update_package(self, cursor, id, maintainer, contact_email, license,
            description, readme, changelog):
        update_package = '''UPDATE packages
                SET maintainer=%s, contact_email=%s, license=%s, description=%s, readme=%s, changelog=%s
                WHERE id=%s'''
        cursor.execute(update_package,
                (maintainer, contact_email, license, description, readme, changelog, id))
        self.db.commit()

    @withcursor
    def get_next_undone_job(self, cursor):
        select_job = 'SELECT id, metadata FROM jobs WHERE finished IS NULL ORDER BY created DESC LIMIT 1'
        cursor.execute(select_job)

        result = cursor.fetchone()
        if result is None:
            return None
        else:
            return jobs.Job.deserialize(result[0], json.loads(result[1]))

    @withcursor
    def create_job(self, cursor, job):
        insert_package = 'INSERT INTO jobs (metadata) VALUES (%s)'

        cursor.execute(insert_package, (json.dumps(job.__dict__),))
        self.db.commit()
        return cursor.lastrowid

    @withcursor
    def finish_job(self, cursor, id):
        update_job = 'UPDATE jobs SET finished=NOW() WHERE id=%s'
        cursor.execute(update_job, (id,))
        self.db.commit()

    @withcursor
    def create_or_update_package_version(self, cursor, package_id, version,
            git_ref, created, platform, architecture, url, dependencies):
        dependencies = json.dumps(dependencies)
        insert = '''INSERT INTO package_versions
                (package_id, version, git_ref, created, platform, architecture, url, dependencies)
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                ON DUPLICATE KEY UPDATE created=%s, url=%s, dependencies=%s'''
        cursor.execute(
                insert,
                (package_id, version, git_ref, created, platform, architecture,
                    url, dependencies, created, url, dependencies))
        self.db.commit()
