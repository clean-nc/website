# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

from twisted.web.template import Element, XMLString, renderer

class BadRequest(Exception):
    pass

class NotFound(Exception):
    pass

class InternalServerError(Exception):
    pass

class TwistedError(Element):
    def __init__(self, message):
        head = open('public/layouts/head.html').read()
        content = open('public/templates/error.html').read()
        foot = open('public/layouts/foot.html').read()
        self.loader = XMLString(head + content + foot)
        self._message = message

    @renderer
    def message(self, request, tag):
        return tag(self._message)
