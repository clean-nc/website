# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

import dateutil.parser
from gitlab import Gitlab
from gitlab.exceptions import GitlabGetError
import json
import re
from semver import VersionInfo

import db

REQUIRED_METADATA = [
        'contact_email',
        'dependencies',
        'description',
        'license',
        'maintainer'
        ]

def run_job():
    try:
        with db.Database() as db_:
            job = db_.get_next_undone_job()
            if job is not None:
                job.run(db_)
    except Exception as e:
        print(e)

class Job:
    def __init__(self, type, **kwargs):
        self.type = type
        self.parameters = kwargs

    @staticmethod
    def deserialize(id, obj):
        if 'type' not in obj:
            raise ValueError('expected type')
        elif 'parameters' not in obj:
            raise ValueError('expected parameters')
        elif obj['type'] == 'update-versions':
            return UpdateVersionsJob.deserialize(id, obj['parameters'])
        else:
            raise ValueError('unknown type ' + obj['type'])

    def run(self, db):
        if self.id is None:
            raise ValueError('cannot run a job without id')

        self._run()

        db.finish_job(self.id)

class UpdateVersionsJob(Job):
    def __init__(self, package_id):
        super().__init__('update-versions', package_id=package_id)

    @staticmethod
    def deserialize(id, parameters):
        if 'package_id' not in parameters:
            raise ValueError('expected package_id')
        else:
            job = UpdateVersionsJob(parameters['package_id'])
            job.id = id
            return job

    def _run(self):
        with db.Database() as db_:
            our_pkg = db_.get_package(id=self.parameters['package_id'])

            if not our_pkg['url'].startswith('https://gitlab.com/'):
                raise ValueError('package url must begin with https://gitlab.com/')

            namespace = our_pkg['url'][19:]

            gl = Gitlab('https://gitlab.com')
            project = gl.projects.get(namespace)

            latest_metadata = None
            latest_version = None
            latest_readme = None
            latest_changelog = None

            for pkg in project.packages.list(iterator=True):
                if pkg.name != our_pkg['name']:
                    continue

                metadata = {}
                try:
                    metadata = project.generic_packages.download(
                            package_name=pkg.name,
                            package_version=pkg.version,
                            file_name='nitrile-metadata.json')
                    metadata = json.loads(metadata)
                except GitlabGetError:
                    continue

                if len([key for key in REQUIRED_METADATA if key not in metadata]) != 0:
                    continue

                version = VersionInfo.parse(pkg.version)
                if latest_metadata is None or latest_version < version:
                    latest_metadata = metadata
                    latest_version = version

                    git_ref = latest_metadata['git_ref'] if 'git_ref' in latest_metadata else f'v{pkg.version}'

                    try:
                        latest_readme = project.files.raw(file_path='README.md', ref=git_ref)
                    except:
                        latest_readme = f'README.md does not exist on {git_ref}.'

                    try:
                        latest_changelog = project.files.raw(file_path='CHANGELOG.md', ref=git_ref)
                    except:
                        latest_changelog = f'CHANGELOG.md does not exist on {git_ref}.'

                for file in pkg.package_files.list(iterator=True):
                    match = re.search(r'^([\w-]*)-([\d.]*)-(linux|mac|windows|any)-(arm64|x64|x86|arm|intel|32bit|64bit|any)\.tar\.gz$', file.file_name)
                    if match is None:
                        continue

                    platform = match.group(3)
                    architecture = match.group(4)

                    if match.group(1) != our_pkg['name']:
                        continue
                    if match.group(2) != pkg.version:
                        continue

                    dependencies = {dep_pkg: dep_cstr for dep_pkg, dep_cstr in metadata['dependencies'].items()}
                    target = platform + '-' + architecture
                    if 'extra_dependencies' in metadata and target in metadata['extra_dependencies']:
                        for dep_pkg, dep_cstr in metadata['extra_dependencies'][target].items():
                            dependencies[dep_pkg] = dep_cstr

                    url = f'https://gitlab.com/{namespace}/-/package_files/{file.id}/download'
                    db_.create_or_update_package_version(
                            our_pkg['id'],
                            pkg.version,
                            metadata['git_ref'] if 'git_ref' in metadata else None,
                            dateutil.parser.parse(pkg.created_at),
                            platform,
                            architecture,
                            url,
                            dependencies)

            if latest_metadata is not None:
                db_.update_package(our_pkg['id'],
                        latest_metadata['maintainer'],
                        latest_metadata['contact_email'],
                        latest_metadata['license'],
                        latest_metadata['description'],
                        latest_readme,
                        latest_changelog)
