# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

from datetime import datetime, timedelta
import humanize
from semver import VersionInfo
from twisted.web.template import Element, XMLString, flattenString, renderer, tags

from db import Database
from exceptions import NotFound

def human_time_delta(date):
    delta = datetime.now() - date
    return humanize.naturaltime(delta)


class PackageVersion(Element):
    def __init__(self, pkg, version):
        self._pkg = pkg
        self._version = version
        self._info = pkg['versions'][version]
        content = open('public/templates/pkg.version.html').read()
        self.loader = XMLString(content)

    def common_dependencies(self):
        targets = list(self._info['targets'].values())

        if len(targets) == 0:
            return []

        common = {pkg: cstr for pkg, cstr in targets[0]['dependencies'].items()}

        for target in targets[1:]:
            common_deps = list(common.items())
            for dep, constraint in common_deps:
                if dep not in target['dependencies'] or \
                        target['dependencies'][dep] != constraint:
                    common.pop(dep)

        return common

    def render_dependencies(self, deps, secondary=False):
        first = True
        for pkg, constraint in deps.items():
            if not first:
                yield '; '
            else:
                first = False

            yield tags.a(pkg, href='/pkg/'+pkg, class_='link-secondary' if secondary else '')
            yield ' '
            if type(constraint) is str:
                yield tags.span(constraint, class_='text-muted')
            elif 'optional' in constraint and constraint['optional']:
                yield tags.span(constraint['version'] + ' (optional)', class_='text-muted')
            else:
                yield tags.span(constraint, class_='text-muted')

    def targets(self):
        common = self.common_dependencies()

        first = True
        for target, info in sorted(self._info['targets'].items()):
            extra_dependencies = {pkg: cstr for pkg, cstr in info['dependencies'].items() if pkg not in common}

            if not first:
                yield '; '
            else:
                first = False

            yield tags.a(target, href=info['url'], target='_blank')

            if len(extra_dependencies) > 0:
                yield tags.small(
                    ' (depends on ',
                    *self.render_dependencies(extra_dependencies, secondary=True),
                    ')',
                    class_='text-muted'
                )

    def badge(self):
        if self._pkg['latest_version'] == self._version:
            return tags.span('latest', class_='badge bg-primary ms-2')

        this_v = VersionInfo.parse(self._version)
        for v in self._pkg['parsed_versions']:
            if this_v.major == v.major and \
                    this_v.minor == v.minor and \
                    this_v.patch < v.patch:
                return None

        return tags.span(
            'latest ' + str(this_v.major) + '.' + str(this_v.minor),
            class_='badge bg-secondary ms-2'
        )

    def git_ref(self):
        # Only return a git_ref if it is the same for all targets
        refs = [t['git_ref'] for t in self._info['targets'].values() if 'git_ref' in t and t['git_ref'] is not None]
        refs = list(set(refs))
        return refs[0] if len(refs) == 1 else None

    @renderer
    def version(self, request, tag):
        created = datetime.fromtimestamp(self._info['created'])

        common_dependencies = self.common_dependencies()
        if len(common_dependencies) > 0:
            common_dependencies = tags.p(
                'Dependencies: ',
                *self.render_dependencies(common_dependencies),
                '.',
                class_='my-1 small'
            )
        else:
            common_dependencies = ''

        git_ref = self.git_ref()
        if git_ref is None:
            version = str(self._version)
        else:
            version = tags.a(
                str(self._version),
                target='_blank',
                href=f'{self._pkg["url"]}/tree/{git_ref}',
                class_='link-dark text-decoration-none'
            )

        badge = self.badge()

        if badge is None:
            tag.attributes['class'] += ' collapse old-version'

        tag.fillSlots(
            version=version,
            badge=badge or '',
            created_date=str(created),
            created_date_delta=human_time_delta(created),
            common_dependencies=common_dependencies,
            targets=self.targets()
        )

        return tag


class Package(Element):
    def __init__(self, name):
        with Database() as db:
            pkg = db.get_package(name=name, with_versions=True)
            # There may be no versions if the package was registered but
            # versions have not been fetched yet, or if an error occurred
            # during fetching them (e.g. private repo).
            if pkg is None or len(pkg['versions']) == 0:
                raise NotFound('no such package')
            self._pkg = pkg

            self._pkg['parsed_versions'] = [VersionInfo.parse(v) for v in self._pkg['versions'].keys()]

        head = open('public/layouts/head.html').read()
        content = open('public/templates/pkg.html').read()
        foot = open('public/layouts/foot.html').read()
        self.loader = XMLString(head + content + foot)

    def sorted_versions(self):
        keys = sorted(
            self._pkg['versions'].keys(),
            key=lambda v: VersionInfo.parse(v),
            reverse=True
        )
        return [PackageVersion(self._pkg, k) for k in keys]

    def version_date(self, version):
        return datetime.fromtimestamp(self._pkg['versions'][version]['created'])

    @renderer
    def info(self, request, tag):
        latest_version_date = self.version_date(self._pkg['latest_version'])

        tag.fillSlots(
            homepage=self._pkg['url'][8:],
            latest_version=self._pkg['latest_version'],
            latest_version_date=str(latest_version_date),
            latest_version_date_delta=human_time_delta(latest_version_date),
            maintainer=self._pkg['maintainer'],
            license=self._pkg['license']
        )

        return tag

    @renderer
    def deprecated(self, request, tag):
        if self._pkg['deprecated']:
            yield tag.clone()

    @renderer
    def name(self, request, tag):
        return tag(self._pkg['name'])

    @renderer
    def description(self, request, tag):
        return tag(self._pkg['description'])

    @renderer
    def readme(self, request, tag):
        tag.fillSlots(
            gitlab_url=self._pkg['url'],
            latest_version=self._pkg['latest_version'],
            readme=self._pkg['readme']
        )

        return tag

    @renderer
    def changelog(self, request, tag):
        tag.fillSlots(
            gitlab_url=self._pkg['url'],
            latest_version=self._pkg['latest_version'],
            readme=self._pkg['changelog']
        )

        return tag

    @renderer
    def versions(self, request, tag):
        return tag(self.sorted_versions())
