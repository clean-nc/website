# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

FROM debian:bookworm-slim as builder
ARG NODE_VERSION=21.5.0
RUN apt update -yy && apt upgrade -yy && apt install curl xz-utils -yy
RUN curl https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz | tar xJ -C /usr --strip-components=1

COPY package-lock.json /build/package-lock.json
COPY package.json /build/package.json

WORKDIR /build
RUN npm ci

FROM alpine:edge

WORKDIR /usr/src/app

COPY setup.cfg /usr/src/app/setup.cfg
COPY setup.py /usr/src/app/setup.py
COPY website /usr/src/app/website
COPY public /usr/src/app/public

COPY --from=builder build/node_modules/bootstrap/dist/js/bootstrap.min.js public/endpoints/js
COPY --from=builder build/node_modules/bootstrap/dist/css/bootstrap.min.css public/endpoints/css
COPY --from=builder build/node_modules/bootstrap/dist/css/bootstrap.min.css.map public/endpoints/css

COPY --from=builder build/node_modules/markdown-it/dist/markdown-it.min.js public/endpoints/js

COPY --from=builder build/node_modules/lunr/lunr.min.js public/endpoints/js

RUN apk update
RUN apk upgrade
RUN apk add py3-pip mariadb-dev build-base python3-dev py3-wheel
RUN pip install --no-cache-dir --break-system-packages .

COPY .env.example /usr/src/app/.env

CMD ["python", "website"]
